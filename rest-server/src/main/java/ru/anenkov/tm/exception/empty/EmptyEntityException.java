package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyEntityException extends AbstractException {

    public EmptyEntityException() {
        super("Error! Entity is Null..");
    }

}
