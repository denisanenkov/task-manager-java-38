package ru.anenkov.tm.exception.rest;

public class NoSuchEntitiesException extends RuntimeException {

    public NoSuchEntitiesException() {
    }

    public NoSuchEntitiesException(String message) {
        super(message);
    }

}
