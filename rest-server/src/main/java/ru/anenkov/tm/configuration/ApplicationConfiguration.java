package ru.anenkov.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.anenkov.tm")
public class ApplicationConfiguration {
}
