package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @Nullable
    Task findOneByIdEntity(@Nullable String id);

    void removeOneById(@Nullable String id);

    void add(@Nullable Task task);

    @Nullable
    List<Task> getList();

    void removeAllTasks();

    long count();

}
