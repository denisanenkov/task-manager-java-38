package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final Project project
    ) {
        if (project == null) throw new EmptyEntityException();
        projectRepository.save(project);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    @Override
    public void removeAllProjects() {
        projectRepository.deleteAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> getList() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return projectRepository.count();
    }

}
