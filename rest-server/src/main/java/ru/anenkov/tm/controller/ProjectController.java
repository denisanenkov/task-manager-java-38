package ru.anenkov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.anenkov.tm.enumerated.Status;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.service.ProjectService;

@Controller
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @ModelAttribute("status")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectService.getList());
    }

    @GetMapping("/project/create")
    public String create() {
        projectService.add(new Project("new Project" + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectService.removeOneById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") Project project,
            BindingResult result
    ) {
        projectService.add(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        Project project = projectService.findOneByIdEntity(id);
        return new ModelAndView("project-edit", "project", project);
    }

}
