package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import ru.anenkov.tm.dto.TaskDTO;

import java.util.List;
import java.util.Optional;

public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @Nullable
    Optional<TaskDTO> findById(@Nullable @Param("id") String id);

    void removeById(@NotNull @Param("id") final String id);

    void removeAllBy();

}
