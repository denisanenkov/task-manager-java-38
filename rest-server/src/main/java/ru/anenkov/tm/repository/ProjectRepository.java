package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.anenkov.tm.model.Project;

import java.util.*;

public interface ProjectRepository extends JpaRepository<Project, String> {


}
